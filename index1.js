var express = require('express');
var app = express();
var Parser = require('./fifa_process');
var DB = require('./DBfunctions');
const axios = require('axios');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.post('/new-message', function (req, res) {
    //var obj = JSON.toString(req.body);
    var parsed_response = Parser.parseMessage(req.body);
    DB.main(parsed_response);

    return res.end();
})



app.listen(3000, function () {
    console.log('started on 3000');
    //DB.test()
});
