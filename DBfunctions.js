var MongoClient = require('mongodb').MongoClient;
var url = "mongodb+srv://user:pass@cluster0-qqgvm.mongodb.net/";
var axios = require('axios');
var Parse = require('./fifa_process')


var save_record_old = exports.save_record = function (pred, collection, chat, text) {
  MongoClient.connect(url, function(err, db) {
    if (err){
	 send_msg(err, chat);
	 throw err;
	}
    var dbo = db.db("fifadb");
    dbo.collection(collection).insertOne(pred, function(err, res) {
      if (err) {
	send_msg("db problem", chat);
	throw err;
	}
      send_msg(text, chat);

      db.close();
    });
  });
};


var save_record = exports.save_record = function (pred, collection, chat, text) {
  MongoClient.connect(url, function(err, db) {
    if (err){
	 // send_msg(err, chat);
	 throw err;
	}
    var dbo = db.db("fifadb");
    var query = {
      user_id: pred["user_id"],
        game1: pred["team1"],
        game2: pred["team2"]
    };
    var update = {
      $set: {
        goal1: pred["goal1"],
          goal2: pred["goal2"]
      }
    };
    dbo.collection(collection).update(query, update, {upsert: true}, function(err, res) {
      if (err) {
	// send_msg("db problem", chat);
	throw err;
	}
      send_msg(text, chat);
      // console.log(res.result.nModified + "modified");
      db.close();
    });
  });
};


var get_all = exports.get_all = function (collection) {
  return new Promise (function (resolve, reject) {
  // console.log("Called!")
  var result = []
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("fifadb");
    dbo.collection(collection).find({}).toArray(function(err, result) {
      if (err) throw err;
      // console.log(result);
      //   console.log(result);
      resolve(result);
      db.close();
    });
  });
})
}

var update_result = exports.update_result = function (result, chatID) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("fifadb");
    var myquery = { team1: result["team1"], team2: result["team2"] };
    var newvalues = { $set: {goal1: result["goal1"], goal2: result["goal2"] } };
    // console.log("here");
    dbo.collection("Games").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      send_msg(res.result.nModified + " games updated", chatID);
      // console.log("worked");
      db.close();
    });
  });
}

var delete_game = exports.delete_game = function (result, chatID) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("fifadb");
    var myquery = { team1: result["team1"], team2: result["team2"] };
    dbo.collection("Games").deleteOne(myquery, function(err, res) {
      if (err) throw err;
      send_msg(res.result.n + " games deleted", chatID);
      db.close();
    });
  });
}

exports.main = function (response) {
        var chatID = response["chat_group_id"];
        // send_msg(response, -266372953);
        if (response["is_valid"]) {
          // console.log("valid");
          var text = response["text"];
          var type = response["type"];

            if (type === "prediction") {
              console.log("pred");
              get_all("Games").then(function (fullfilled) {
                  var games = fullfilled;
                  // console.log(games);
                  var rv = Parse.isPredictionValid(response, games);
                  // console.log(rv);
                  if (rv) {
                    text = response["text"];
                    // save_record(response["prediction"], "Predictions", chatID, text);
                      save_record_old(response["prediction"], "Predictions", chatID, text);
                  }
                  else {
                    send_msg(response["text"], chatID);
                  }
              });

            }
            if (type === "adminprediction") {
              save_record_old(response["prediction"], "Predictions", chatID, text);
            }
            if (type === "game") {
              save_record_old(response["game"], "Games", chatID, text);
            }
            if (type === "result") {
              update_result(response["game"], chatID)
                // save_record(response["game"], "Games", chatID, text)
            }
            if (type === "help") {
              send_msg(text, chatID);
            }
            if (type === "delete") {
              delete_game(response["game"], chatID)
            }
            if (type === "leaderboard") {
              get_all("Predictions").then(function(fullfilled) {
                var predictions = fullfilled;
                get_all("Games").then(function(fullfilled) {
                  var games = fullfilled;
                  console.log(predictions);
                  console.log(games);
                  text = Parse.buildLadderTable(predictions, games);
                  console.log(text);
                  send_msg(text, chatID);
                })
              });

            }
            if (type === "register") {
              send_msg(response, -266372953);
              send_msg(text, chatID);
            }
            if (type === "unknown") {
              send_msg(text, chatID);
            }

}
}


var send_msg = exports.send_msg = function(msg, chat) {

	axios.post('https://api.telegram.org/bot619805132:AAFNoxAi-DMJIEkE7NhysL7jaHifJ-BIBqA/sendMessage', {
            chat_id: chat,
            text: msg
        })
            .then(response => {

            })
            .catch(err => {

            })
}
